import { Test, TestingModule } from '@nestjs/testing';
import { CommentaryController } from './commentary.controller';

describe('Commentary Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [CommentaryController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: CommentaryController = module.get<CommentaryController>(CommentaryController);
    expect(controller).toBeDefined();
  });
});
