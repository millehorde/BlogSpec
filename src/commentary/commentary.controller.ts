import { Controller } from '@nestjs/common';
import {CommentaryService} from './commentary.service';
import {ApiUseTags, ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('commentary')
@Controller('commentary')
export class CommentaryController {
    constructor(private readonly commentaryService: CommentaryService) {
    }
}
