import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import {CommentaryController} from './commentary.controller';
import {User} from '../entity/User';
import {CommentaryService} from './commentary.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Commentary} from '../entity/Commentary';
import {Article} from '../entity/Article';
import {AuthMiddleware} from '../user/auth.middleware';

@Module({
    imports: [TypeOrmModule.forFeature([User, Article, Commentary])],
    controllers: [CommentaryController],
    providers: [CommentaryService],
    exports: [CommentaryService],
})
export class CommentaryModule implements NestModule {
    public configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .forRoutes(
                {path: 'commentary/post', method: RequestMethod.POST},
                {path: 'commentary/delete', method: RequestMethod.DELETE});
    }
}
