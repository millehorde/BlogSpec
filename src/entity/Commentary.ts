import {Entity, PrimaryGeneratedColumn, Column, OneToOne} from 'typeorm';
import {User} from './User';
import {Article} from './Article';

@Entity()
export class Commentary {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255, nullable: false })
    content: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    created: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    updated: Date;

    @OneToOne(type  => User, user => user.id)
    authorId: User;

    @OneToOne(type  => Article, article => article.id)
    articleId: Article;
}
