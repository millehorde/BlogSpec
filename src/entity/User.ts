import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert} from 'typeorm';
import * as bcrypt from 'bcrypt';

export enum UserType {
    ADMIN = 'admin',
    AUTHOR = 'author',
    STANDARD = 'standard',
}

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255, unique: true, nullable: false })
    email: string;

    @Column({ length: 255, default: null  })
    password: string;

    @Column({ length: 255, default: null })
    firstName: string;

    @Column({ length: 255, default: null })
    lastName: string;

    @Column({
      type: 'enum',
      enum: UserType,
      default: UserType.STANDARD,
    })
    type: UserType;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    created: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    updated: Date;

    @Column({ nullable: true, default: null }) // TODO Avatar profile
    avatar: string;

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }
}
