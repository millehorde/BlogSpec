import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

export enum UserType {
    ADMIN = 'admin',
    AUTHOR = 'author',
}

@Entity()
export class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255, unique: true, nullable: false })
    titre: string;

    @Column({ length: 255, nullable: false })
    content: string;

    @Column({
      type: 'enum',
      enum: UserType,
      default: UserType.AUTHOR,
    })
    type: UserType;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    created: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    updated: Date;
}
