import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import 'reflect-metadata';
import {SwaggerModule, DocumentBuilder} from '@nestjs/swagger';
import {createConnection} from 'typeorm';
import {Logger} from '@nestjs/common';

const port = process.env.PORT || 8080;

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    // swagger config
    const options = new DocumentBuilder()
        .setTitle('Kilou Blog ')
        .setDescription('The Kiloublog API description')
        .setVersion('1.0')
        .addTag('kilouBlog')
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api', app, document);

    // TypeOrm config
    await app.listen(port);
    Logger.log(`Server start on http://localhost:${port}`);
}
bootstrap();
