import {HttpException} from '@nestjs/common/exceptions/http.exception';
import {NestMiddleware, HttpStatus, Injectable, Logger} from '@nestjs/common';
import {ExtractJwt, Strategy} from 'passport-jwt';
import {Request, Response, NextFunction} from 'express';
import * as jwt from 'jsonwebtoken';
import {SECRET} from '../config';
import {UserService} from './user.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(private readonly userService: UserService) {
    }

    resolve(): (req: Request, res: Response, next: NextFunction) => void {
        return async (req: Request, res: Response, next: NextFunction) => {
            if (!req.headers.authorization) {
                throw new HttpException('You should be logged.', HttpStatus.UNAUTHORIZED);
            }
            const token = (req.headers.authorization as string);
            const decoded = jwt.verify(token, SECRET);
            if (!decoded) {
                throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
            }
            const user = await this.userService.getOneByMail(decoded.email);
            if (!user) {
                throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
            }
            req.user = user;
            next();
        };
    }
}
