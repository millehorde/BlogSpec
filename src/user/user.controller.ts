import {
    Body,
    Controller,
    FilesInterceptor,
    Get,
    HttpException,
    Param,
    Post, Put,
    Req,
    UploadedFile,
    UseInterceptors,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiUseTags,
} from '@nestjs/swagger';
import {UserService} from './user.service';
import {LoginUserDto, UpdateUser, UserDTO, UserRO} from './User.dto';
import {extname} from 'path';
import {diskStorage} from 'multer';

@ApiBearerAuth()
@ApiUseTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {
    }

    @Post('register')
    @ApiOperation({title: 'Create new user'})
    @UsePipes(new ValidationPipe())
    async register(@Body() body: UserDTO) {
        return this.userService.create(body);
    }

    @Post('login')
    @ApiOperation({title: 'Login a user'})
    @UsePipes(new ValidationPipe())
    async login(@Body() loginUserDto: LoginUserDto): Promise<UserRO> {
        const body = await this.userService.getOneByMail(loginUserDto.email);
        if (!body) {
            throw new HttpException({user: 'User not found'}, 401);
        }
        const passwordIsCorrect = await this.userService.comparePassword(loginUserDto.password, body.password);
        if (!passwordIsCorrect) {
            throw new HttpException({user: 'Wrong password'}, 401);
        }

        const token = await UserService.generateJWT(body);
        const {email, firstName, lastName, type} = body;
        const user = {email, token, firstName, lastName, type};
        return {user};
    }

    @Get('get')
    @ApiOperation({title: 'Get all user'})
    @UsePipes(new ValidationPipe())
    async getAll() {
        return this.userService.getAll();
    }

    @Get('get/:email')
    @ApiOperation({title: 'Get a user with email'})
    @UsePipes(new ValidationPipe())
    getOneByMail(@Param() body) {
        return this.userService.getOneByMail(body.email);
    }

    @Put('avatar/:email')
    @ApiOperation({title: 'Upload avatar'})
    @UseInterceptors(FilesInterceptor('avatar', 1, {
        storage: diskStorage({
            destination: './upload',
            filename: (req, file, cb) => {
                const date = new Date();
                const finalDate = date.getDay() + '-' + date.getMonth() + '-' + date.getFullYear();
                file.finalName = req.params.email + '_' + finalDate + extname(file.originalname);
                return cb(null, file.finalName);
            },
        }),
    }))
    async upload(@Req() req, @UploadedFile() file) {
        // return this.userService.setAvatar(req.params.email, file);
    }

    @Get('currentInfos')
    @ApiOperation({title: 'Get current user'})
    async getCurrentUser(@Req() req) {
        return req.user;
    }

    @Put('update')
    @ApiOperation({title: 'Update user datas'})
    @UsePipes(new ValidationPipe())
    updateUserInfos(@Req() req) {
        return this.userService.update(req);
    }
}
