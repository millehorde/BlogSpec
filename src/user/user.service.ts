import {HttpStatus, Injectable, Logger} from '@nestjs/common';
import {User} from '../entity/User';
import { Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {UpdateUser, UserDTO} from './User.dto';
import * as bcrypt from 'bcrypt';
import {SECRET} from '../config';

const jwt = require('jsonwebtoken');

@Injectable()
export class UserService {
    constructor(@InjectRepository(User) private usersRepository: Repository<User>) {
    }

    async create(user: UserDTO) {
        const isExist = await this.userIsExist(user.email);
        if (isExist) {
            return {
                code: HttpStatus.BAD_REQUEST,
                message: 'BAD REQUEST : User already exist',
            };
        }
        user.password = await bcrypt.hash(user.password, 10);
        return await this.usersRepository.save(user);
    }

    async getAll() {
        return this.usersRepository.find();
    }

    public async getOneByMail(param: string) {
        return this.usersRepository.findOne({
            where: {email: param},
        });
    }

    async userIsExist(param: string) {
        return this.usersRepository.count({
            where: {email: param},
        });
    }

    async comparePassword(attempt: string, password: string): Promise<boolean> {
        return await bcrypt.compare(attempt, password);
    }

    static async generateJWT(user) {
        const today = new Date();
        const exp = new Date(today);
        exp.setDate(today.getDate() + 60);
        return jwt.sign({
            username: user.username,
            email: user.email,
            firstname: user.firstname,
            lastname: user.lastname,
            isAuthor: user.isAuthor,
            exp: exp.getTime() / 1000,
        }, SECRET);
    }

    public async setAvatar(userEmail: string, path: string) {
        const user = await this.getOneByMail(userEmail);
        if (!user) {
            return {
                code: HttpStatus.BAD_REQUEST,
                message: 'BAD REQUEST : User not found',
            };
        }
        return await this.usersRepository.update({avatar: path}, user);
    }

    public async update(user: UpdateUser) {
        return await this.usersRepository.update({id: user.id}, user);
    }
}
