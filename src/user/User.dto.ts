import {IsNotEmpty, IsNumber, IsString} from 'class-validator';
import {UserType} from '../entity/User';

export class UserDTO {
    @IsNotEmpty()
    @IsString()
    email: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsNotEmpty()
    @IsString()
    firstName: string;

    @IsNotEmpty()
    @IsString()
    lastName: string;

    @IsNotEmpty()
    isAuthor: boolean;

    avatar: string;
}

export class UserRO {
    user: UserData;
}

export class UserData {
    email: string;

    token: string;

    firstName: string;

    lastName: string;

    type: UserType;
}

export class LoginUserDto {

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}

export class UpdateUser {
    @IsNotEmpty()
    @IsNumber()
    id: number;

    @IsNotEmpty()
    @IsString()
    email: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsNotEmpty()
    @IsString()
    firstName: string;

    @IsNotEmpty()
    @IsString()
    lastName: string;
}
