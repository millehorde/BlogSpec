import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import {UserController} from './user.controller';
import {UserService} from './user.service';
import {User} from '../entity/User';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AuthMiddleware} from './auth.middleware';

@Module({
    imports: [TypeOrmModule.forFeature([User])],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService],
})
export class UserModule implements NestModule {
    public configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            .forRoutes(
                {path: 'user/avatar', method: RequestMethod.PUT},
                {path: 'user/currentInfos', method: RequestMethod.GET},
                {path: 'user/update', method: RequestMethod.PUT});
    }
}
