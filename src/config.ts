export const SECRET = 'ON_SE_MARRE_CHEZ_YNOV_PUTAIN';
export const SERVER_URL: string  =  'http://localhost:8080/';
import { User } from 'src/entity/User';

declare global {
    namespace Express {
        export interface Request {
            user: User;
        }
    }
}
