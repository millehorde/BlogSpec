import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UserModule} from './user/user.module';
import {ArticleModule} from './article/article.module';
import { CommentaryService } from './commentary/commentary.service';
import { CommentaryModule } from './commentary/commentary.module';

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        UserModule,
        ArticleModule,
        CommentaryModule,
    ],
    providers: [CommentaryService],
})
export class AppModule {
}
