import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {Article} from '../entity/Article';
import {InjectRepository} from '@nestjs/typeorm';

@Injectable()
export class ArticleService {
    constructor(@InjectRepository(Article) private articleRepository: Repository<Article>) {
    }

    public async getAll() {
        return this.articleRepository.find();
    }

    public async getOneById(param: number) {
        return this.articleRepository.find({id: param});
    }
}
