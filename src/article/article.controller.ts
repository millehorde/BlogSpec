import {Controller, Get, Param} from '@nestjs/common';
import {ArticleService} from './article.service';
import {ApiUseTags, ApiBearerAuth, ApiOperation} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('article')
@Controller('article')
export class ArticleController {
    constructor(private readonly articleService: ArticleService) {
    }

    @Get('get')
    @ApiOperation({title: 'Get all articles'})
    async getAll() {
        return this.articleService.getAll();
    }

    @Get('get/:id')
    @ApiOperation({title: 'Get 1 article'})
    async get(@Param() body) {
        return this.articleService.getOneById(body.id);
    }
}
